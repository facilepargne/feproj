<?php

namespace App\Controller;

use App\Repository\StockRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(StockRepository $stockRepo)
    {
        $stock = $stockRepo->findOneByName('produit');
        
        return $this->render('home/homepage.html.twig', 
            [
                'stock' => $stock,
            ]);
    }
}
