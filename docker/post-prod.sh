#!/bin/bash

echo "=====>>> Préparation du .env local en production..."
docker-compose run --rm php-fpm composer dump-env prod
echo "=====>>> Installation des bibliothèques tierces..."
docker-compose run --rm php-fpm composer install --no-dev --optimize-autoloader;
echo "=====>>> Effacement du cache..."
docker-compose run --rm php-fpm php bin/console cache:clear
echo "=====>>> Mise à jour des assets..."
docker-compose run --rm php-fpm yarn install
docker-compose run --rm php-fpm yarn encore production
echo "=====>>> Mise à jour de la structure de la BDD..."
docker-compose run --rm php-fpm php bin/console doctrine:migrations:migrate